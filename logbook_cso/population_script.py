'''	A script to initally populate the database for testing.
	Note that there may be unexpected behaviour with duplicate entries '''

import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE',
                      'logbook_cso.settings')

import django

django.setup()

import datetime

from django.contrib.auth.models import User
from main.models import *
from django.template.defaultfilters import slugify

# values for gender
MALE_CHOICE = 'M'
FEMALE_CHOICE = 'F'
OTHER_CHOICE = 'O'

# values for priority
GREEN_PRIORITY = 'green'
AMBER_PRIORITY = 'yellow'
RED_PRIORITY = 'red'

# values for service type
EMPLOYABILITY_CHOICE = 'EMP'
RESIDENCE_CHOICE = 'RES'
VOLUNTEERING_CHOICE = 'VOL'

# Risk level choices
LOW_RISK = 1
MEDIUM_RISK = 2
HIGH_RISK = 3


def populate():
    # ---Users and Superusers---#

    # add users
    # inbuilt django function
    # User.objects.create_user(name, email, password, first_name=first_name, last_name=last_name)

    staff = User.objects.filter(username="normal")
    if staff.exists():
        staff = staff[0]
    else:
        staff = User.objects.create_user("normal", "normal@normal.com", "normal", first_name="normal",
                                         last_name="mcnormalson")

    # add superusers
    # inbuilt django function
    # User.objects.create_superuser(name, email, password, first_name=first_name, last_name=last_name)

    admin_staff = User.objects.filter(username="admin")
    if admin_staff.exists():
        admin_staff = admin_staff[0]
    else:
        User.objects.create_superuser("admin", "admin@admin.com", "admin", first_name="admin", last_name="mcadminson")

    # ---Referrals---#

    # add referrals
    # create_referral(nin, first_name, last_name, phone, dob, address, referral_date, referred_by,
    #	 	contact_name, contact_phone, gender, reason, notes, doctor_name, doctor_phone,
    #	 	surgery_address, is_banned)

    create_referral("QQ123456C", "max", "stirner", "123456789", datetime.date(1997, 12, 13), "17 Lilybank Gardens",
                    datetime.date(2007, 12, 6), "me",
                    "thomas hobbes", "987654321", MALE_CHOICE, "default reason", "", "ohashi junko", "56709",
                    "20 someplace somewhere", False)

    # ---Services---#

    # add services
    # create_service(service_type, description)

    emp_service = create_service("Employability Service", "Jobs for everyone yay")
    hos_service = create_service("Hostel Service")
    create_service("Residence Service")
    create_service("Volunteering Service")

    # ---Service Users---#
	
    # add service users
    # create_service_user(date_accepted, health_issues, mental_issues, drug_issues, criminal_background,
    #		alcohol_issues, emotional_issues, is_pregnant, referral)
	
    referral = Referral.objects.filter(first_name="max", last_name="stirner")[0]
    user = create_service_user(datetime.date(2017, 12, 6), "", "", "", "",
                                "", "", False, referral)

	# some automatic user generation with generic names and much the same details as max
	# however age and gender are altered (date accepted is also slightly different)
	# users of ages 16 to 22 are generated
	# default gender is male, on even iterations it becomes female and on odd iterations divisible by 3 it becomes other/non-specified
    for i in range(1, 8):
        for j in range(i):
            gender = MALE_CHOICE # default
            if(not (i % 2)): # on even iterations
                gender = FEMALE_CHOICE
            elif(not (i % 3)): # on odd iterations divisible by 3
                gender = OTHER_CHOICE

            referral = create_referral("QQ123456C", "user_" + str(i) + str(j), "generic", "123456789", 
			                            datetime.date(datetime.date.today().year - 15 - i, datetime.date.today().month, datetime.date.today().day), 
                                        "17 Lilybank Gardens", datetime.date(2007, 12, 6), 
                                        "me", "thomas hobbes", "987654321", gender, "default reason", "", "ohashi junko", "56709",
                                        "20 someplace somewhere", False)

            user = create_service_user(datetime.date(2017, 11, 6), "", "", "", "",
                                        "", "", False, referral)

    # ---Service-User Relationships---#

    # add service-user relationships
    # create_user_service_relationship(service, service_user)

    create_user_service_relationship(emp_service, user)

    # ---Log Entries---#

    # add log entries
    # (priority, title, description, action_required, assigned_user,
    #		service_users, previous_log_entry)

    assigned_user = User.objects.filter(username="normal")[0]
    prev = create_log_entry(GREEN_PRIORITY, "green entry", "this is an entry coloured green", False, assigned_user,
                            ServiceUser.objects.none(), None)

    assigned_user = User.objects.filter(username="admin")[0]
    service_users = ServiceUser.objects.filter(referral=Referral.objects.filter(first_name="max", last_name="stirner"))
    log = create_log_entry(RED_PRIORITY, "red entry", "this is an entry coloured red", True, assigned_user,
                           service_users, prev)

    assigned_user = User.objects.filter(username="normal")[0]
    create_log_entry(AMBER_PRIORITY, "amber entry", "this is an entry coloured amber", False, assigned_user,
                     ServiceUser.objects.none(), None)

    assigned_user = User.objects.filter(username="admin")[0]
    log = create_log_entry(AMBER_PRIORITY, "older amber pr", "this is an entry coloured amber and is older", True,
                           assigned_user,
                           service_users, timestamp="2018-01-21", service=hos_service)
    # ---Diary Entries---#

    # add diary entries
    # create_diary_entry(appointment_date, details, service_user, staff_user)

    create_diary_entry(datetime.date(2018, 4, 1), "this is a diary entry", user, staff)
	
	#---Incident Reports---#
	
    # add incident report
    # create_incident_report(title, incident_date, incident_time, location, brief_description, witnesses,
    #                         cause, description, action_taken, police_details, medical_assistance_details,
    #                         debriefing_meeting_date, comments_action_taken, comments_action_required,
    #                         brief_proposed_action, log_entry)
					
    create_incident_report("incident report", datetime.date(2018, 4, 1), datetime.datetime.now().time(), "here", "an incident", "yousaf", "something",
                                "it was bad", "no", "police investigated", "medical something", datetime.date(2018, 4, 2),
                                "no action", "never action", "do not action", log)
	
	#---Risk Assessments---#
	
	#add risk assessments
	#create_risk_assessment(review_date, activity, hazards, existing_control, additional_control,
	#					initial_risk, residual_risk, service_users, staff_users, log_entry)
	
    staff_mems = User.objects.filter(username="normal")
    create_risk_assessment(datetime.date(2018, 4, 1), "no", "no hazards", "not present", "none", LOW_RISK, LOW_RISK, service_users, 
                            staff_mems, log)
	
# function to add referral to database
def create_referral(nin, first_name, last_name, phone, dob, address, referral_date, referred_by,
                    contact_name, contact_phone, gender, reason, notes, doctor_name, doctor_phone,
                    surgery_address, is_banned):
					
    # primary key not involved so we will create a new object unless there's an exact match of all fields
    entry = Referral.objects.get_or_create(nin=nin,
                                           first_name=first_name,
                                           last_name=last_name,
                                           phone=phone,
                                           dob=dob,
                                           address=address,
                                           referral_date=referral_date,
                                           referred_by=referred_by,
                                           contact_name=contact_name,
                                           contact_phone=contact_phone,
                                           gender=gender,
                                           reason=reason,
                                           notes=notes,
                                           doctor_name=doctor_name,
                                           doctor_phone=doctor_phone,
                                           surgery_address=surgery_address,
                                           is_banned=is_banned)[0]

    return entry
	
#function to add service to database
def create_service(service_type, description=None):

    # primary key not involved so we will create a new object unless there's an exact match of all fields
    entry = Service.objects.get_or_create(service_type=service_type,
                                          description=description)[0]

    return entry

# function to add service user to database
def create_service_user(date_accepted, health_issues, mental_issues, drug_issues, criminal_background,
                        alcohol_issues, emotional_issues, is_pregnant, referral):
    # primary key not involved so we will create a new object unless there's an exact match of all fields
    entry = ServiceUser.objects.get_or_create(date_accepted=date_accepted,
                                              health_issues=health_issues,
                                              mental_issues=mental_issues,
                                              drug_issues=drug_issues,
                                              criminal_background=criminal_background,
                                              alcohol_issues=alcohol_issues,
                                              emotional_issues=emotional_issues,
                                              is_pregnant=is_pregnant,
                                              referral=referral)[0]

    return entry


# function to add user/service relationship to database
def create_user_service_relationship(service, service_user):
    entry = UserUsesService()

    entry.service = service
    entry.service_user = service_user
    entry.save()
    return entry


# function to add log entry to database
def create_log_entry(priority, title, description, action_required, assigned_user,
                     service_users, previous_log_entry=None, timestamp=None, service=None):
    # primary key not involved so we will create a new object unless there's an exact match of all fields
    entry = LogEntry.objects.get_or_create(priority=priority,
                                           title=title,
                                           description=description,
                                           action_required=action_required,
                                           assigned_user=assigned_user)[0]

    if timestamp:
        entry.timestamp = timestamp
    if service:
        entry.service = service
    entry.service_users = service_users
    entry.previous_log_entry = previous_log_entry
    # entry.timestamp = datetime.datetime(2017, 12, 1, 0, 0, 0)
    entry.save()

    return entry


# function to add diary entry to database
def create_diary_entry(appointment_date, details, service_user, staff_user):
    # use update_or_create to tackle not null constraint
    # primary key not involved so we will create a new object unless there's an exact match of all fields
    entry = DiaryEntry.objects.update_or_create(appointment_date=appointment_date,
                                                details=details,
                                                defaults={'service_user': service_user,
                                                          'staff_user': staff_user})[0]

    return entry


# function to add incident report to database
def create_incident_report(title, incident_date, incident_time, location, brief_description, witnesses,
                           cause, description, action_taken, police_details, medical_assistance_details,
                           debriefing_meeting_date, comments_action_taken, comments_action_required,
                           brief_proposed_action, log_entry):
    # use update_or_create to tackle not null constraint
    # primary key not involved so we will create a new object unless there's an exact match of all fields
    entry = IncidentReport.objects.update_or_create(title=title,
                                                    incident_date=incident_date,
                                                    incident_time=incident_time,
                                                    location=location,
                                                    brief_description=brief_description,
                                                    witnesses=witnesses,
                                                    cause=cause,
                                                    description=description,
                                                    action_taken=action_taken,
                                                    police_details=police_details,
                                                    medical_assistance_details=medical_assistance_details,
                                                    debriefing_meeting_date=debriefing_meeting_date,
                                                    comments_action_taken=comments_action_taken,
                                                    comments_action_required=comments_action_required,
                                                    brief_proposed_action=brief_proposed_action,
                                                    defaults={'log_entry': log_entry})[0]

    return entry


# function to add risk assessment to database
def create_risk_assessment(review_date, activity, hazards, existing_control, additional_control,
                           initial_risk, residual_risk, assessed_service_users, staff_users, log_entry):
    # use update_or_create to tackle not null constraint
    # primary key not involved so we will create a new object unless there's an exact match of all fields
    entry = RiskAssessment.objects.update_or_create(review_date=review_date,
                                                    activity=activity,
                                                    hazards=hazards,
                                                    existing_control=existing_control,
                                                    additional_control=additional_control,
                                                    initial_risk=initial_risk,
                                                    residual_risk=residual_risk,
                                                    defaults={'log_entry': log_entry})[0]

    entry.assessed_service_users = assessed_service_users
    entry.staff_users = staff_users
    entry.save()
    return entry


# call populate
print("Running pop script...")
populate()
print("Population complete!")
