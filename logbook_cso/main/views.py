from .models import *
from .forms import *
import datetime
from django.template import RequestContext
from django.template import loader
from django.shortcuts import render, get_object_or_404, render_to_response
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.db.models import Q, QuerySet
from collections import OrderedDict

# graph imports
import matplotlib as mpl

# Use Agg backend to avoid problems when reloading plots
mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
# file path creation imports
from django.conf import settings
from os.path import join
from os.path import normpath
# file name import
from uuid import uuid4
# user sorting
from django.db.models import Max
from django.db.models import Min
# numpy import for vectorised calculations
import numpy as np


# Create your views here.
# home function that authenticates the username and password for the login page
def home(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        #if the user is authentic, log them in
        if user:
            login(request, user)
            return HttpResponseRedirect('/main/logbook/')
        #else show the following message
        else:
            print("Invalid signin details: {0}, {1}".format(username, password))
            return render(request, 'main/homepage1.html', {'invalid': True})
    elif not request.user.is_authenticated():
        return render(request, 'main/homepage1.html', {})
    else:
        return HttpResponseRedirect('/main/logbook/')




# A helper function used to check if a log has any of the folowing entities linked to it
# incident report, risk assessment, neighbour complaint, social welfare, child protection, adult concern
def form_checker(forms):
    for form in forms:
        if form:
            return True
    return False


# Display logs with the following information in order:
# Title, colour, timestamp, service users, description, staff user, links
@login_required
def logbook(request):
    # Get search query, if any
    query = request.POST.get("search_text", '')
    search_date = request.POST.get("search_date", '')
    sort_by = request.POST.get("sort_by", '')
    search_priority = request.POST.get("search_priority", '')
    search_service = request.POST.get("search_service", '')

    if query or search_date:
        query_words = query.split()

        # Filter by date, if applicable
        if search_date:
            logs = LogEntry.objects.filter(timestamp__date=search_date)
        else:
            logs = LogEntry.objects.all()

        # Get matches for each search word separately
        # Return only logs where all the search words match one of the log fields
        for q in query_words:
            logs = logs.filter(Q(title__icontains=q) |
                               Q(description__icontains=q) |
                               Q(assigned_user__username__icontains=q) |
                               Q(service_users__referral__first_name__icontains=q) |
                               Q(service_users__referral__last_name__icontains=q)
                               )

    else:
        logs = LogEntry.objects.all()

    # Get the right priority if applicable
    if search_priority:
        logs = logs.filter(priority=search_priority)

    # Get the right service if applicable
    if search_service:
        logs = logs.filter(service__service_type=search_service)

    # Order the elements
    if sort_by:
        logs = logs.order_by(sort_by)
    else:
        logs = logs.order_by('-timestamp')

    # Construct a dictionary with dates as keys
    # Must use an ordered dict to preserve the sorting
    date_dict = OrderedDict()
    if logs:
        # Set current date to the first logs date
        current = logs[0].timestamp.date()
        temp_list = []
        for log in logs:

            # check if the following entities exist for a log
            # used to determine if 'Show Additional Details' link is needed
            incident = IncidentReport.objects.filter(log_entry=log)
            risk = RiskAssessment.objects.filter(log_entry=log)
            neighbour = NeighbourComplaints.objects.filter(log_entry=log)
            social = SocialWork.objects.filter(log_entry=log)
            child = ChildProtectionConcern.objects.filter(log_entry=log)
            adult = VulnerableYoungAdultConcerns.objects.filter(log_entry=log)
            forms = [incident, risk, neighbour, social, child, adult]

            #if the logs date is equal to the current date
            if (log.timestamp.date() == current):

                # append the log to the temp list and
                #whether or not it is linked to any of the entities mentioned above 
                temp_list.append({'log': log, 'forms': form_checker(forms)})
            else:

                #add the list of logs to the dictionary with the key being the current date
                date_dict[str(current)] = temp_list[:]
                #clear the temp_list
                del temp_list[:]
                #reset the current date
                current = log.timestamp.date()

                temp_list.append({'log': log, 'forms': form_checker(forms)})

        date_dict[str(current)] = temp_list[:]

    return render(request, 'main/logbook.html', {'logs': date_dict, 'services': Service.objects.all()})


# A function used to get and show all the entities linked to a log
# Data loaded through javascript
@login_required
def log_details(request):
    data = dict()
    context = dict()
    log_id = None
    if request.method == 'GET':
        log_id = request.GET['log']
    #if successful GET request check if log exists
    if log_id:
        log = LogEntry.objects.get(id=log_id)

        # try and excepts to check if an entity exists for that log
        # if it does, add to the context dictionary
        try:
            incident = IncidentReport.objects.get(log_entry=log)
        except IncidentReport.DoesNotExist:
            incident = None
        context['incident'] = incident

        try:
            risk = RiskAssessment.objects.get(log_entry=log)
        except RiskAssessment.DoesNotExist:
            risk = None
        context['risk'] = risk

        try:
            neighbour = NeighbourComplaints.objects.get(log_entry=log)
        except NeighbourComplaints.DoesNotExist:
            neighbour = None
        context['neighbour'] = neighbour

        try:
            social = SocialWork.objects.get(log_entry=log)
        except SocialWork.DoesNotExist:
            social = None
        context['social'] = social

        try:
            child = ChildProtectionConcern.objects.get(log_entry=log)
        except ChildProtectionConcern.DoesNotExist:
            child = None
        context['child'] = child

        try:
            adult = VulnerableYoungAdultConcerns.objects.get(log_entry=log)
        except VulnerableYoungAdultConcerns.DoesNotExist:
            adult = None
        context['adult'] = adult

    # Render the data from the context dictionary using the 'log_details.html' template
    data['html_log'] = render_to_string('main/log_details.html', context, request=request, )

    # return it as a Json response
    return JsonResponse(data)

# A function used for the rendering of the calander page
# Checking if events exists for a certain date
# Returning the event details
def diary(request):
    data = dict()
    event_date = None
    check_event = None
    # events = None
    if request.method == 'GET':
        # event date used to return the events for a certain date
        # event_date is a specific date
        event_date = request.GET.get('date')
        # check event used to check if an event exists for a certain date
        # check_event is a month and year
        check_event = request.GET.get('check')

        if event_date:
            # split the event date to year, month and date and convert it to date time
            year, month, day = map(int, event_date.split("-"))
            event_date = datetime.date(year, month, day)
            # get all the DiaryEntry objects that exist for the event_date
            events = DiaryEntry.objects.filter(appointment_date__contains=event_date)
            #render the events data using the 'diary_event.html' template
            data['event_date'] = render_to_string('main/diary_event.html', {'events': events}, request=request, )
            return JsonResponse(data)
        
        elif check_event:
            event_list = []
            # array of months with 30 and 31 days
            m_31 = [1, 3, 5, 7, 8, 10, 12]
            m_30 = [4, 6, 9, 11]
            # split check_event into month and year
            year, month = map(int, check_event.split("-"))
            #if month has 31 days
            if (month in m_31):
                # for each day in the month
                for i in range(1, 32):
                    # check if that date has any DiaryEvents and appent to event_list
                    if (DiaryEntry.objects.filter(appointment_date__contains=datetime.date(year, month, i))):
                        event_list.append(check_event + "-" + str(i))
            # if month has 30 days
            elif (month in m_30):
                # for each day in the month
                for i in range(1, 31):
                    # check if that date has any DiaryEvents and appent to event_list
                    if (DiaryEntry.objects.filter(appointment_date__contains=datetime.date(year, month, i))):
                        event_list.append(check_event + "-" + str(i))
            else:
                # for the month of Febuary check if year is leap year
                if (year % 400 == 0) or ((year % 4 == 0) and (year % 100 != 0)):
                    # for each day in the month
                    for i in range(1, 30):
                        # check if that date has any DiaryEvents and appent to event_list
                        if (DiaryEntry.objects.filter(appointment_date__contains=datetime.date(year, month, i))):
                            event_list.append(check_event + "-" + str(i))
                else:
                    # for each day in the month
                    for i in range(1, 29):
                        # check if that date has any DiaryEvents and appent to event_list
                        if (DiaryEntry.objects.filter(appointment_date__contains=datetime.date(year, month, i))):
                            event_list.append(check_event + "-" + str(i))

            # Add the events list to a dictionary
            data['events'] = {'event_list': event_list}
            #return dictionary as Json response
            return JsonResponse(data)

    # render the 'diary.html' template
    return render_to_response("main/diary.html")

#The following few functions are used to render forms for different entities

# Form for a Log entry
@login_required
def log_form(request):
    context = dict()
    if request.method == 'POST':
        log_form = LogForm(data=request.POST)

        if log_form.is_valid():
            log_data = log_form.save(commit=False)
            # set the assigned user to the current user
            log_data.assigned_user = User.objects.get(id=request.user.id)
            #save the form
            log_data.save()
            # As we do save(commit=False) we need to do save_m2m() to save any many to many attributes
            log_form.save_m2m()

            return HttpResponseRedirect('/main/logbook')
    else:
        log_form = LogForm()

    context['log_form'] = log_form

    #render the form
    response = render(request, 'main/log_form.html', context)
    return response

# Form for an Incident report
@login_required
def incident_form(request):
    context = dict()
    if request.method == 'POST':
        incident_form = IncidentForm(data=request.POST)

        if incident_form.is_valid():
            #save form
            incident = incident_form.save(commit=True)

            return HttpResponseRedirect('/main/logbook')
    else:
        incident_form = IncidentForm()

    context['incident_form'] = incident_form
    #render form
    response = render(request, 'main/incident_form.html', context)
    return response

#form for a risk assessment
@login_required
def risk_form(request):
    context = dict()
    if request.method == 'POST':
        risk_form = RiskForm(data=request.POST)

        if risk_form.is_valid():
            #save form
            risk = risk_form.save(commit=True)

            return HttpResponseRedirect('/main/logbook')
    else:
        risk_form = RiskForm()

    context['risk_form'] = risk_form
    #render form
    response = render(request, 'main/risk_form.html', context)
    return response


# form to add a referral
@login_required
def referral_form(request):
    context = dict()
    if request.method == 'POST':
        referral_form = ReferralForm(data=request.POST)

        if referral_form.is_valid():
            #save form
            referral_form.save(commit=True)

            return HttpResponseRedirect('/main/logbook')
    else:
        referral_form = ReferralForm()

    context['referral_form'] = referral_form
    #render form
    response = render(request, 'main/referral_form.html', context)
    return response

#form to add a service user
@login_required
def service_user_form(request, def_ref=None):
    context = dict()
    if request.method == 'POST':
        service_user_form = ServiceUserForm(data=request.POST)
        # form to set the service of a user
        user_uses_service_form = UserUsesServiceForm(data=request.POST)

        #if both forms are valid
        if service_user_form.is_valid() and user_uses_service_form.is_valid():
            #save service_user form
            service_user = service_user_form.save(commit=True)

            user_uses_service = user_uses_service_form.save(commit=False)
            #set the service of service user
            user_uses_service.service_user = service_user
            #save user uses service form
            user_uses_service.save()

            return HttpResponseRedirect('/main/logbook')
    else:
        # Check if a default referral was passed as a parameter
        if def_ref:
            service_user_form = ServiceUserForm(initial={"referral": Referral.objects.get(id=def_ref)})
        else:
            service_user_form = ServiceUserForm()
        user_uses_service_form = UserUsesServiceForm()

    context['service_user_form'] = service_user_form
    context['user_uses_service_form'] = user_uses_service_form
    #render forms
    response = render(request, 'main/service_user_form.html', context)
    return response


#form to add diary entry
@login_required
def diary_entry_form(request):
    context = dict()
    if request.method == 'POST':
        diary_entry_form = DiaryEntryForm(data=request.POST)

        if diary_entry_form.is_valid():
            #save form
            diary_entry_form.save(commit=True)

            return HttpResponseRedirect('/main/diary')
    else:
        diary_entry_form = DiaryEntryForm()

    context['diary_entry_form'] = diary_entry_form
    #render form
    response = render(request, 'main/diary_entry_form.html', context)
    return response


#form to add neighbour complaint
@login_required
def neighbour_complaint_form(request):
    context = dict()
    if request.method == 'POST':
        neighbour_complaint_form = NeighbourComplaintForm(data=request.POST)

        if neighbour_complaint_form.is_valid():
            #save form
            neighbour_complaint_form.save(commit=True)

            return HttpResponseRedirect('/main/logbook')
    else:
        neighbour_complaint_form = NeighbourComplaintForm()

    context['neighbour_complaint_form'] = neighbour_complaint_form
    #render form
    response = render(request, 'main/neighbour_complaint_form.html', context)
    return response

#form to add vulnerable_young_adult_concerns entry
@login_required
def vulnerable_young_adult_concerns_form(request):
    context = dict()
    if request.method == 'POST':
        vulnerable_young_adult_concerns_form = VulnerableYoungAdultConcernsForm(data=request.POST)

        if vulnerable_young_adult_concerns_form.is_valid():
            #save form
            vulnerable_young_adult_concerns_form.save(commit=True)

            return HttpResponseRedirect('/main/logbook')
    else:
        vulnerable_young_adult_concerns_form = VulnerableYoungAdultConcernsForm()

    context['vulnerable_young_adult_concerns_form'] = vulnerable_young_adult_concerns_form
    #render form
    response = render(request, 'main/vulnerable_young_adult_concerns_form.html', context)
    return response


# form to add social_work entry
@login_required
def social_work_form(request):
    context = dict()
    if request.method == 'POST':
        social_work_form = SocialWorkForm(data=request.POST)

        if social_work_form.is_valid():
            #save form
            social_work_form.save(commit=True)

            return HttpResponseRedirect('/main/logbook')
    else:
        social_work_form = SocialWorkForm()

    context['social_work_form'] = social_work_form
    #render form
    response = render(request, 'main/social_work_form.html', context)
    return response


# form to add child protection entry
@login_required
def child_protection_concern_form(request):
    context = dict()
    if request.method == 'POST':
        child_protection_concern_form = ChildProtectionConcernForm(data=request.POST)

        if child_protection_concern_form.is_valid():
            #save form
            child_protection_concern_form.save(commit=True)

            return HttpResponseRedirect('/main/logbook')
    else:
        child_protection_concern_form = ChildProtectionConcernForm()

    context['child_protection_concern_form'] = child_protection_concern_form
    #render form
    response = render(request, 'main/child_protection_concern_form.html', context)
    return response


#function used to show data of a service user and show all logs for a service
@login_required
def service_user_page(request, id):
    service_user = get_object_or_404(ServiceUser, id=id)
    services = UserUsesService.objects.filter(service_user=service_user)
    logs = LogEntry.objects.filter(service_users__id=service_user.id)
    temp_list = []
    for log in logs:
        incident = IncidentReport.objects.filter(log_entry=log)
        risk = RiskAssessment.objects.filter(log_entry=log)
        neighbour = NeighbourComplaints.objects.filter(log_entry=log)
        social = SocialWork.objects.filter(log_entry=log)
        child = ChildProtectionConcern.objects.filter(log_entry=log)
        adult = VulnerableYoungAdultConcerns.objects.filter(log_entry=log)
        forms = [incident, risk, neighbour, social, child, adult]

        temp_list.append({'log': log, 'forms': form_checker(forms)})

    return render(request, 'main/Service_User.html',
                  {'service_user': service_user, 'logs': temp_list, 'services': services})


def referral_page(request, id):
    referral = get_object_or_404(Referral, id=id)
    # If the referral has already been accepted, show the service user instead
    if hasattr(referral, 'serviceuser'):
        return HttpResponseRedirect('/main/service_user/'+str(referral.serviceuser.id))
    return render(request, 'main/referral.html', {'referral': referral})


'''A helper function to calculate age given birth date.
	For graphing, age is necessary but only birth dates are stored.'''


def get_age(birth_date):
    correction = 0  # used to correct for when birthday has not yet happened this year so directly subtracting years is off by one

    if (datetime.date.today().month < birth_date.month):  # if before birthday month this year
        correction = 1  # subtract 1 from age

    elif (datetime.date.today().month == birth_date.month):  # else if currently birthday month
        if (datetime.date.today().day < birth_date.day):  # if before birthday day this month
            correction = 1  # subtract 1 from age

    return datetime.date.today().year - birth_date.year - correction


'''A page to display graphs of relevant information in the DB.
	Currently:
	Number of service users who joined each month for a given year.
	Number of users total separated by gender
	Number of users total separated by age
	Number of users total separated by age and gender'''


@login_required
def graphs(request):

    # make sure there are service users before trying to make graphs
    if not ServiceUser.objects.all().exists():
        return render(request, 'main/graphs.html', {'no_service_users': True})

    # initialise context dictionary
    context = dict()

    # initialise year form
    year_form = YearForm()

    # matplotlib graph sketching
    fig = plt.figure(figsize=(10, 10))

    # if form data being sent
    if request.method == 'POST':
        year_form = YearForm(data=request.POST)

        ### residents over time ###
        if year_form.is_valid():

            # filter database by date
            year = int((year_form.cleaned_data)["year"])  # year to plot graph over
            boundaries = []  # list of date boundaries to simplify counting new service users in each time range

            # generate boundaries - these are sorting buckets
            for month in range(12):
                boundaries += [datetime.datetime(year, month + 1, 1)]
            boundaries += [datetime.datetime(year + 1, 1, 1)]

            # take count of new service users in each time range
            service_users = []  # value used on x-axis of graph
            for i in range(12):
                service_users += [(ServiceUser.objects.filter(date_accepted__gte=boundaries[i],
                                                              date_accepted__lt=boundaries[i + 1])).count()]

            # create subplot for our figure
            ax = fig.add_subplot(2, 2, 1)

            # plot scatter
            plt.plot(range(12), service_users)

            # set title
            ax.set_title("Number of New Service Users Over Time")
            # months along x-axis
            ax.set_xlabel("Months")
            ax.set_xticks(range(12))
            ax.set_xticklabels(["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"])
            # service users along y-axis
            ax.set_ylabel("New Service Users")
            ax.set_ylim(min(service_users) - 1, max(service_users) + 1)
            # Be sure to only pick integer tick locations.
            ax.yaxis.set_major_locator(ticker.MaxNLocator(integer=True))

    # get only referrals that correspond to an active service user (used in all following graph sketches)
    active_users = Referral.objects.filter(serviceuser__isnull=False)

    ### service-users by gender ###

    # take count of service users in each age bracket
    service_users_M = (active_users.filter(gender='M')).count()
    service_users_F = (active_users.filter(gender='F')).count()
    service_users_O = (active_users.filter(gender='O')).count()

    # create subplot for our figure
    ax = fig.add_subplot(2, 2, 2)

    # plot bar graph
    male = ax.bar([1], service_users_M, label="Male", color='#0000FF')
    female = ax.bar([2], service_users_F, label="Female", color='#FFC0CB')
    other = ax.bar([3], service_users_O, label="Other/Non-Disclosed", color='#808080')

    # set title
    ax.set_title("Number of Service Users by Gender")
    # months along x-axis
    ax.set_xlabel("Gender")
    ax.set_xticks([1, 2, 3])
    ax.set_xticklabels(['Male', 'Female', 'Other/Non-Disclosed'])
    # service users along y-axis
    ax.set_ylabel("Service Users")

    ### service-users by age ###

    # limits of ages
    lowest_age = get_age(active_users.aggregate(Max('dob'))['dob__max'])
    highest_age = get_age(active_users.aggregate(Min('dob'))['dob__min'])
    # range of ages (x-axis)
    ages = range(lowest_age, highest_age + 1)

    # take count of service users in each age bracket
    service_users = []  # value used on x-axis of graph
    for age in ages:
        startDate = datetime.date(datetime.date.today().year - age - 1, datetime.date.today().month,
                                  datetime.date.today().day)
        endDate = datetime.date(datetime.date.today().year - age, datetime.date.today().month,
                                datetime.date.today().day)
        service_users += [(active_users.filter(dob__gt=startDate,
                                               dob__lte=endDate)).count()]

    # create subplot for our figure
    ax = fig.add_subplot(2, 2, 3)

    # plot bar graph
    ax.bar(ages, service_users, label="Age")

    # set title
    ax.set_title("Number of Service Users by Age")
    # months along x-axis
    ax.set_xlabel("Age")
    ax.set_xticks(ages)
    # service users along y-axis
    ax.set_ylabel("Service Users")

    ### service-users by age and gender ###

    service_users_M = []
    service_users_F = []
    service_users_O = []
    for age in ages:
        startDate = datetime.date(datetime.date.today().year - age - 1, datetime.date.today().month,
                                  datetime.date.today().day)
        endDate = datetime.date(datetime.date.today().year - age, datetime.date.today().month,
                                datetime.date.today().day)
        service_users_M += [(active_users.filter(gender='M',
                                                 dob__gt=startDate,
                                                 dob__lte=endDate)).count()]
        service_users_F += [(active_users.filter(gender='F',
                                                 dob__gt=startDate,
                                                 dob__lte=endDate)).count()]
        service_users_O += [(active_users.filter(gender='O',
                                                 dob__gt=startDate,
                                                 dob__lte=endDate)).count()]

    # create subplot for our figure
    ax = fig.add_subplot(2, 2, 4)

    # plot bar graph
    male = ax.bar(np.array(ages) - 0.2, service_users_M, label="Male", width=0.2, color='#0000FF')
    female = ax.bar(np.array(ages), service_users_F, label="Female", width=0.2, color='#FFC0CB')
    other = ax.bar(np.array(ages) + 0.2, service_users_O, label="Other/Non-Disclosed", width=0.2, color='#808080')

    # set title
    ax.set_title("Number of Service Users by Age and Gender")
    # months along x-axis
    ax.set_xlabel("Age")
    ax.set_xticks(ages)
    # service users along y-axis
    ax.set_ylabel("Service Users")

    # save file locally
    # set subdirectories of static part of file path (directly used in template)
    fileend = normpath(join("images/", "graphimg_" + ".png"))
    context['graph'] = fileend
    # set rest of path
    filename = normpath(join(settings.BASE_DIR, 'static/', fileend))
    # save file
    fig.savefig(filename)

    # for permanent saving use this as the name
    # "graphimg_" + str(uuid4()) + ".png")

    # pass on form to context dict
    context['year_form'] = year_form

    return render(request, 'main/graphs.html', context)


@login_required
def service_users_list(request):
    search_query = request.POST.get("search_text", "")
    sort_by = request.POST.get("sort_by", "")

    service_users = ServiceUser.objects.all()

    # Search by query if applicable
    if search_query:
        query_words = search_query.split()
        # Search for all query words - same as log search
        for q in query_words:
            # Look for query in first name and last name
            service_users = service_users.filter(
                Q(referral__first_name__icontains=q) | Q(referral__last_name__icontains=q))

    # Order the elements
    if sort_by:
        service_users = service_users.order_by(sort_by)
    else:
        service_users = service_users.order_by('referral__last_name')

    return render(request, 'main/service_users_list.html', {'service_users': service_users})


@login_required
def referrals_list(request):
    search_query = request.POST.get("search_text", "")
    sort_by = request.POST.get("sort_by", "")

    referrals = Referral.objects.filter(serviceuser__isnull=True)

    # Search by query if applicable
    if search_query:
        query_words = search_query.split()
        # Search for all query words - same as log search
        for q in query_words:
            # Look for query in first name and last name
            referrals = referrals.filter(
                Q(first_name__icontains=q) | Q(last_name__icontains=q))

    # Order the elements
    if sort_by:
        referrals = referrals.order_by(sort_by)
    else:
        referrals = referrals.order_by('last_name')

    return render(request, 'main/referrals_list.html', {'referrals': referrals})


#Logs user out
@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('home'))
