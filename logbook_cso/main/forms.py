from django import forms
from .models import *
from datetime import datetime as dt


# class to add date input
class DateInput(forms.DateInput):
    input_type = 'date'

	
#class to add time input
class TimeInput(forms.TimeInput):
    input_type = 'time'

	
class LogForm(forms.ModelForm):

  class Meta:
    model = LogEntry
    fields = '__all__'
    exclude = ('timestamp','assigned_user',)

	
class IncidentForm(forms.ModelForm):


  class Meta:
    model = IncidentReport
    fields = '__all__'
    widgets = {'incident_date': DateInput(), 'debriefing_meeting_date': DateInput(),'incident_time': TimeInput()}

	
#a form that takes a year from within the last five years from the user
class YearForm(forms.Form):

  year = forms.ChoiceField(choices=[(dt.now().year - y, dt.now().year - y) for y in range(6)])
	
	
class RiskForm(forms.ModelForm):

  class Meta:
    model = RiskAssessment
    fields = '__all__'
    widgets = {'review_date': DateInput()}

	
class ReferralForm(forms.ModelForm):

  class Meta:
    model = Referral
    fields = '__all__'
    widgets = {'dob': DateInput(),'referral_date': DateInput()}

	
class ServiceUserForm(forms.ModelForm):

  class Meta:
    model = ServiceUser
    fields = '__all__'
    widgets = {'date_accepted': DateInput()}

class UserUsesServiceForm(forms.ModelForm):

  class Meta:
    model = UserUsesService
    fields = ('service',)

	
class DiaryEntryForm(forms.ModelForm):

  class Meta:
    model = DiaryEntry
    fields = '__all__'
    widgets = {'appointment_date': DateInput()}

	
class NeighbourComplaintForm(forms.ModelForm):

  class Meta:
    model = NeighbourComplaints
    fields = '__all__'
    exclude = ('current_date',)
    widgets = {'complaint_date': DateInput(), 'complaint_time': TimeInput()}

	
class SocialWorkForm(forms.ModelForm):

  class Meta:
    model = SocialWork
    fields = '__all__'
    exclude = ('current_date',)

	
class VulnerableYoungAdultConcernsForm(forms.ModelForm):

  class Meta:
    model = VulnerableYoungAdultConcerns
    fields = '__all__'
    exclude = ('current_date',)
    widgets = {'date_of_birth': DateInput(), 'informed_date': DateInput()}

	
class ChildProtectionConcernForm(forms.ModelForm):

  class Meta:
    model = ChildProtectionConcern
    fields = '__all__'
    widgets = {'date_of_birth': DateInput(), 'mothers_date_of_birth': DateInput(), 'fathers_date_of_birth': DateInput(), 'date_of_concern': DateInput(), 'informed_date': DateInput()}
