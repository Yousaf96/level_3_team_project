from django.conf.urls import url
from django.conf import settings
from django.contrib.auth.views import password_reset, password_reset_done, password_reset_confirm, \
    password_reset_complete
from . import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^logbook/$', views.logbook, name='logbook'),
    url(r'^log_details/$', views.log_details, name='log_details'),
    url(r'log_form/$', views.log_form, name='log_form'),
    url(r'diary/$', views.diary, name='diary'),
    url(r'diary_event/$', views.diary, name='diary_event'),
    url(r'^service_user/(\d+)/', views.service_user_page, name='service_user_page'),
    url(r'^referral/(\d+)/', views.referral_page, name='referral_page'),
    url(r'graphs/$', views.graphs, name='graphs'),
    url(r'incident_form/$', views.incident_form, name='incident_form'),
    url(r'risk_form/$', views.risk_form, name='risk_form'),
    url(r'referral_form/$', views.referral_form, name='referral_form'),
    url(r'service_user_form/$', views.service_user_form, name='service_user_form'),
    url(r'service_user_form/(?P<def_ref>\d+)/$', views.service_user_form),
    url(r'diary_entry_form/$', views.diary_entry_form, name='diary_entry_form'),
    url(r'neighbour_complaint_form/$', views.neighbour_complaint_form, name='neighbour_complaint_form'),
    url(r'social_work_form/$', views.social_work_form, name='social_work_form'),
    url(r'vulnerable_young_adult_concerns_form/$', views.vulnerable_young_adult_concerns_form,
        name='vulnerable_young_adult_concerns_form'),
    url(r'child_protection_concern_form/$', views.child_protection_concern_form, name='child_protection_concern_form'),
    url(r'service_users_list/$', views.service_users_list, name='service_users_list'),
    url(r'referrals_list/$', views.referrals_list, name='referrals_list'),
    url(r'^logout/$', views.user_logout, name='logout'),
    url(r'^password_reset/$', password_reset, name='password_reset'),
    url(r'^password_reset/done/$', password_reset_done, name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', password_reset_confirm,
        name='password_reset_confirm'),
    url(r'^reset/done/$', password_reset_complete, name='password_reset_complete'),
]
