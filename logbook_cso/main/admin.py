from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Referral)
admin.site.register(Service)
admin.site.register(ServiceUser)
admin.site.register(UserUsesService)
admin.site.register(LogEntry)
admin.site.register(DiaryEntry)
admin.site.register(IncidentReport)
admin.site.register(RiskAssessment)
admin.site.register(NeighbourComplaints)
admin.site.register(SocialWork)
admin.site.register(VulnerableYoungAdultConcerns)
admin.site.register(ChildProtectionConcern)

admin.site.site_url = '/main/logbook/'