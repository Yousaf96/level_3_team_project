from django.test import TestCase, Client
from main.models import *
from django.contrib import auth

import datetime


# Test models
class ReferralTest(TestCase):
    def setUp(self):
        Referral.objects.create(nin=666, first_name="John", last_name="O'John", phone="123212", dob="1980-01-01",
                                referral_date="2018-01-01", address="123 Fake Street", gender="M")

    def test_referral_name_display(self):
        test_ref = Referral.objects.get(nin=666)
        self.assertEqual(test_ref.__str__(), "John O'John")


class ServiceTest(TestCase):
    def setUp(self):
        Service.objects.create(service_type="Evil rituals", description="Evil")

    def test_service_display(self):
        test_serv = Service.objects.get(service_type="Evil rituals")
        self.assertEqual(test_serv.__str__(), "Evil rituals")


class ServiceUserTest(TestCase):
    def setUp(self):
        test_ref = Referral.objects.create(nin=1337, first_name="Mary", last_name="Yram", phone="345678",
                                           dob="1982-01-01",
                                           referral_date="2018-01-01", address="123 Fake Street", gender="F")
        ServiceUser.objects.create(date_accepted="2018-01-01", referral=test_ref)

    def test_service_user_display(self):
        test_servuser = ServiceUser.objects.get(referral__nin=1337)
        self.assertEqual(test_servuser.__str__(), "Mary Yram")


class LogEntryTest(TestCase):
    def setUp(self):
        test_ref = Referral.objects.create(nin=1337, first_name="Mary", last_name="Yram", phone="345678",
                                           dob="1982-01-01",
                                           referral_date="2018-01-01", address="123 Fake Street", gender="F")
        test_servuser = ServiceUser.objects.create(date_accepted="2018-01-01", referral=test_ref)
        staff_user = User.objects.create(username="Asdas")
        test_prev_log = LogEntry.objects.create(priority="green", title="A potato was stolen", assigned_user=staff_user,
                                                service_users=[test_servuser], id=1)
        LogEntry.objects.create(priority="red", title="We found the potato", assigned_user=staff_user,
                                service_users=[test_servuser], previous_log_entry=test_prev_log, id=2)
        IncidentReport.objects.create(log_entry=test_prev_log, title="Potatoes can be stolen",
                                      incident_date=datetime.date.today(), incident_time=datetime.datetime.now().time(),
                                      debriefing_meeting_date=datetime.date.today())

    def test_previous_log(self):
        test_log = LogEntry.objects.get(title="We found the potato")
        self.assertTrue(test_log.has_previous_log_entry())

    def test_log_display(self):
        test_log = LogEntry.objects.get(title="We found the potato")
        self.assertEqual(test_log.__str__(), "We found the potato")

    def test_log_incident_report_display(self):
        test_increp = IncidentReport.objects.get(title="Potatoes can be stolen")
        self.assertEqual(test_increp.__str__(), "Potatoes can be stolen")


class DiaryEntryTest(TestCase):
    def setUp(self):
        test_ref = Referral.objects.create(nin=1337, first_name="Mary", last_name="Yram", phone="345678",
                                           dob="1982-01-01",
                                           referral_date="2018-01-01", address="123 Fake Street", gender="F")
        test_servuser = ServiceUser.objects.create(date_accepted="2018-01-01", referral=test_ref)
        staff_user = User.objects.create(username="Asdas")
        DiaryEntry.objects.create(
            appointment_date=datetime.date.today(), details="We must discuss potato theft",
            service_user=test_servuser,
            staff_user=staff_user, id=1)

    def test_diary_entry_display(self):
        test_de = DiaryEntry.objects.get(id=1)
        self.assertEqual(str(test_de.__str__()), str(test_de.appointment_date))


# Test views
class LoginPageTest(TestCase):
    def setUp(self):
        User.objects.create_user(username="Turnip", password="haggis")

    def test_correct_login(self):
        c = Client()
        response = c.post("/main/", {"username": "Turnip", "password": "haggis"}, follow=True)
        self.assertContains(response, "Search for log entries")

    def test_incorrect_login(self):
        c = Client()
        response = c.post("/main/", {"username": "Evil", "password": "Turnip"}, follow=True)
        self.assertContains(response, "Invalid login details")

    def test_inactive_user(self):
        c = Client()
        test_usr = User.objects.get(username="Turnip")
        test_usr.is_active = False
        test_usr.save()
        response = c.post("/main/", {"username": "Turnip", "password": "haggis"}, follow=True)
        self.assertNotContains(response, "Search for log entries")

    def test_login_page_get(self):
        c = Client()
        response = c.get("/main/")
        self.assertContains(response, "Welcome to OpenBook")

    def test_logout(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        self.assertIn("_auth_user_id", c.session)
        c.get("/main/logout/")
        self.assertNotIn("_auth_user_id", c.session)

    def test_user_already_logged_in(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        response = c.get("/", follow=True)
        self.assertContains(response, "Search for log entries")


class LogbookTest(TestCase):
    def setUp(self):
        User.objects.create_user(username="Turnip", password="haggis")
        test_ref = Referral.objects.create(nin=1337, first_name="Mary", last_name="Yram", phone="345678",
                                           dob="1982-01-01",
                                           referral_date="2018-01-01", address="123 Fake Street", gender="F")
        test_servuser = ServiceUser.objects.create(date_accepted="2018-01-01", referral=test_ref)
        staff_user = User.objects.create(username="Asdas")
        test_prev_log = LogEntry.objects.create(priority="green", title="A potato was stolen", assigned_user=staff_user,
                                                service_users=[test_servuser], id=1)
        LogEntry.objects.create(priority="red", title="We found the potato", assigned_user=staff_user,
                                service_users=[test_servuser], previous_log_entry=test_prev_log, id=2)
        older_log = LogEntry.objects.create(priority="red", title="We ate the potato", assigned_user=staff_user,
                                            service_users=[test_servuser],
                                            id=3)
        older_log.timestamp = datetime.datetime.now() - datetime.timedelta(1)
        older_log.save()
        IncidentReport.objects.create(log_entry=test_prev_log, title="Potatoes can be stolen",
                                      incident_date=datetime.date.today(), incident_time=datetime.datetime.now().time(),
                                      debriefing_meeting_date=datetime.date.today())

    def test_log_no_search(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        response = c.get("/main/logbook", follow=True)
        self.assertContains(response, "stolen")
        self.assertContains(response, "found")
        self.assertContains(response, "ate")

    def test_log_search_words(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        response = c.post("/main/logbook/", {'search_text': 'ate the'})
        self.assertNotContains(response, "stolen")
        self.assertNotContains(response, "found")
        self.assertContains(response, "ate the")

    def test_log_search_date(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        response = c.post("/main/logbook/", {'search_date': (datetime.datetime.now() - datetime.timedelta(1)).date()})
        self.assertNotContains(response, "stolen")
        self.assertNotContains(response, "found")
        self.assertContains(response, "ate the")

    def test_log_search_priority_sorted(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        response = c.post("/main/logbook/", {'search_priority': 'green', 'sort_by': 'timestamp'})
        self.assertContains(response, "stolen")
        self.assertNotContains(response, "found")
        self.assertNotContains(response, "ate the")

    def test_log_search_service(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        response = c.post("/main/logbook/", {'search_service': 'no service'})
        self.assertNotContains(response, "stolen")
        self.assertNotContains(response, "found")
        self.assertNotContains(response, "ate the")

    def test_log_details(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        response = c.get("/main/log_details/", {"log": 1})
        self.assertContains(response, "Potatoes can be stolen")
        self.assertNotContains(response, "Risk assessment")

    def test_log_details_empty(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        response = c.get("/main/log_details/", {"log": 2})
        self.assertNotContains(response, "Incident Report")
        self.assertNotContains(response, "Risk assessment")


class FormsTest(TestCase):
    def setUp(self):
        staff_user = User.objects.create_user(username="Turnip", password="haggis")
        test_ref = Referral.objects.create(nin=1337, first_name="Mary", last_name="Yram", phone="345678",
                                           dob="1982-01-01",
                                           referral_date="2018-01-01", address="123 Fake Street", gender="F")
        test_servuser = ServiceUser.objects.create(date_accepted="2018-01-01", referral=test_ref)
        test_serv = Service.objects.create(service_type="help people")
        UserUsesService.objects.create(service=test_serv, service_user=test_servuser)
        LogEntry.objects.create(priority="green", title="A potato was stolen", assigned_user=staff_user,
                                service_users=[test_servuser], id=1)

    def test_log_form_get(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        response = c.get("/main/log_form/")
        self.assertContains(response, "Add Log")

    def test_log_form_post(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        test_su = ServiceUser.objects.get(referral__nin=1337)
        c.post("/main/log_form/",
               {"priority": ["green"], "title": ["Something happened"], "service_users": [str(test_su.id)]})
        self.assertTrue(LogEntry.objects.filter(title="Something happened"))

    def test_incident_form_get(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        response = c.get("/main/incident_form/")
        self.assertContains(response, "Add Incident")

    def test_incident_form_post(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        test_log = LogEntry.objects.get(title="A potato was stolen")
        c.post("/main/incident_form/", {"title": ["Stolen potato"], "incident_date": [datetime.date.today()],
                                        "incident_time": [datetime.datetime.now().time()],
                                        "debriefing_meeting_date": ["2018-05-05"], "log_entry": [str(test_log.id), ]})
        self.assertTrue(IncidentReport.objects.filter(title="Stolen potato"))

    def test_risk_form_get(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        response = c.get("/main/risk_form/")
        self.assertContains(response, "Add Risk")

    def test_risk_form_post(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        test_su = ServiceUser.objects.get(referral__nin=1337)
        staff_user = User.objects.get(username="Turnip")
        test_log = LogEntry.objects.get(title="A potato was stolen")
        c.post("/main/risk_form/", {"review_date": [datetime.date.today()], "date_assessed": [datetime.date.today()],
                                    "initial-date_assessed": [datetime.date.today()],
                                    "activity": ["Stealing potatoes"], "initial_risk": ["2"], "residual_risk": ["2"],
                                    "log_entry": [str(test_log.id)], "assessed_service_users": [str(test_su.id)],
                                    "staff_users": [str(staff_user.id)]})
        self.assertTrue(RiskAssessment.objects.all())

    def test_neighbour_complaint_form_get(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        response = c.get("/main/neighbour_complaint_form/")
        self.assertContains(response, "Add Neighbour")

    def test_neighbour_complaint_form_post(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        staff_user = User.objects.get(username="Turnip")
        test_log = LogEntry.objects.get(title="A potato was stolen")
        c.post("/main/neighbour_complaint_form/", {"log_entry": [str(test_log.id)], "staff_user": [str(staff_user.id)],
                                                   "complaint_date": [datetime.date.today()],
                                                   "complaint_time": [datetime.datetime.now().time()],
                                                   "complainant_name": ["Angry Neighbour"],
                                                   "recieved_by": [str(staff_user.id)]})
        self.assertTrue(NeighbourComplaints.objects.all())

    def test_vulnerable_adult_form_get(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        response = c.get("/main/vulnerable_young_adult_concerns_form/")
        self.assertContains(response, "Add Vulnerable")

    def test_vulnerable_adult_form_post(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        test_log = LogEntry.objects.get(title="A potato was stolen")
        c.post("/main/vulnerable_young_adult_concerns_form/",
               {"log_entry": [str(test_log.id)], "support_worker": ["Some One"]})
        self.assertTrue(VulnerableYoungAdultConcerns.objects.all())

    def test_social_work_form_get(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        response = c.get("/main/social_work_form/")
        self.assertContains(response, "Add Social")

    def test_social_work_form_post(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        staff_user = User.objects.get(username="Turnip")
        test_log = LogEntry.objects.get(title="A potato was stolen")
        c.post("/main/social_work_form/", {"log_entry": [str(test_log.id)], "staff_user": [str(staff_user.id)],
                                           "office_contacted": ["Ofyce"], "social_worker": ["aesdf"]})
        self.assertTrue(SocialWork.objects.all())

    def test_child_protection_form_get(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        response = c.get("/main/child_protection_concern_form/")
        self.assertContains(response, "Add Child")

    def test_child_protection_form_post(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        test_log = LogEntry.objects.get(title="A potato was stolen")
        c.post("/main/child_protection_concern_form/", {"log_entry": [str(test_log.id)], "raiser_name": ["Con Cerned"]})
        self.assertTrue(ChildProtectionConcern.objects.all())

    def test_referral_form_get(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        response = c.get("/main/referral_form/")
        self.assertContains(response, "Add Referral")

    def test_referral_form_post(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        Service.objects.get(service_type="help people")
        c.post("/main/referral_form/",
               {"dob": [datetime.date.today()], "referral_date": [datetime.date.today()], "first_name": ["Njmbajq"],
                "last_name": ["John"], "address": ["666 Satan Street, Hell"], "gender": ["M"]})
        self.assertTrue(Referral.objects.filter(first_name="Njmbajq"))

    def test_service_user_form_get(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        response = c.get("/main/service_user_form/")
        self.assertContains(response, "Add Service")

    def test_service_user_form_get_def_ref(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        response = c.get("/main/service_user_form/1/")
        self.assertContains(response, "selected>Mary")

    def test_servuce_user_form_post(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        test_new_ref = Referral.objects.create(first_name="Xc", last_name="Cx", dob=datetime.date.today(),
                                               referral_date=datetime.date.today(), address="Lilydank garderns")
        c.post("/main/service_user_form/",
               {"referral": [str(test_new_ref.id)], "date_accepted": [datetime.date.today()], "service": ["1"]})
        self.assertTrue(ServiceUser.objects.filter(referral__first_name="Xc"))

    def test_diary_entry_form_get(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        response = c.get("/main/diary_entry_form/")
        self.assertContains(response, "Add Diary")

    def test_diary_entry_form_post(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        test_su = ServiceUser.objects.get(referral__nin=1337)
        staff_user = User.objects.get(username="Turnip")
        c.post("/main/diary_entry_form/", {"appointment_date": [datetime.date.today()], "details": ["The meeting is"],
                                           "service_user": [str(test_su.id)], "staff_user": [str(staff_user.id)]})
        self.assertTrue(DiaryEntry.objects.filter(details="The meeting is"))

class OtherPagesTest(TestCase):
    def setUp(self):
        staff_user = User.objects.create_user(username="Turnip", password="haggis")
        test_ref = Referral.objects.create(nin=1337, first_name="Mary", last_name="Yram", phone="345678",
                                           dob="1982-12-31",
                                           referral_date="2018-01-01", address="123 Fake Street", gender="F")
        test_ref1 = Referral.objects.create(nin=37, first_name="Af", last_name="Ad", phone="345678",
                                           dob=datetime.date.today()+datetime.timedelta(days=1),
                                           referral_date="2018-01-01", address="123 Fake Street", gender="F")
        Referral.objects.create(nin=7654, first_name="Nota", last_name="Usher", phone="32678",
                                dob="1982-12-31",
                                referral_date="2018-01-01", address="1234 Fake Street", gender="M")
        serv_user = ServiceUser.objects.create(date_accepted="2018-01-01", referral=test_ref)
        ServiceUser.objects.create(date_accepted="2018-01-01", referral=test_ref1)
        LogEntry.objects.create(priority="green", title="A potato was stolen", assigned_user=staff_user,
                                service_users=[serv_user], id=1)
        DiaryEntry.objects.create(appointment_date = datetime.date.today(), service_user=serv_user, staff_user=staff_user, details="Event details")
        DiaryEntry.objects.create(appointment_date="2019-05-01", service_user=serv_user, staff_user=staff_user,
                                  details="Event details")
        DiaryEntry.objects.create(appointment_date="2019-04-01", service_user=serv_user, staff_user=staff_user,
                                  details="Event details")
        DiaryEntry.objects.create(appointment_date="2020-02-01", service_user=serv_user, staff_user=staff_user,
                                  details="Event details")
        DiaryEntry.objects.create(appointment_date="2019-02-01", service_user=serv_user, staff_user=staff_user,
                                  details="Event details")

    def test_service_user_page(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        test_su = ServiceUser.objects.get(referral__nin=1337)
        response = c.get("/main/service_user/" + str(test_su.id) +"/")
        self.assertContains(response, "Mary")
        self.assertContains(response, "Yram")

    def test_diary(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        response = c.get("/main/diary/")
        self.assertContains(response, "Today is")

    def test_diary_event(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        response = c.get("/main/diary_event/", {"date": datetime.date.today()})
        self.assertContains(response, "Event details")

    def test_diary_check_event(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        # Test no events
        # 31 day month
        response = c.get("/main/diary_event/", {"check": "2017-05"})
        self.assertContains(response, "event_list")
        # 30 day month
        response = c.get("/main/diary_event/", {"check": "2017-04"})
        self.assertContains(response, "event_list")
        # 29 day month
        response = c.get("/main/diary_event/", {"check": "2016-02"})
        self.assertContains(response, "event_list")
        # 28 day month
        response = c.get("/main/diary_event/", {"check": "2015-02"})
        self.assertContains(response, "event_list")
        # Test one event
        response = c.get("/main/diary_event/", {"check": str(datetime.date.today().year)+"-"+str(datetime.date.today().month)})
        self.assertContains(response, "event_list")
        # 31 day month
        response = c.get("/main/diary_event/", {"check": "2019-05"})
        self.assertContains(response, "05-1")
        # 30 day month
        response = c.get("/main/diary_event/", {"check": "2019-04"})
        self.assertContains(response, "04-1")
        # 29 day month
        response = c.get("/main/diary_event/", {"check": "2020-02"})
        self.assertContains(response, "02-1")
        # 28 day month
        response = c.get("/main/diary_event/", {"check": "2019-02"})
        self.assertContains(response, "02-1")

    def test_graphs(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        response = c.get("/main/graphs/")
        self.assertContains(response, "Choose a Year")

    def test_graphs_post(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        response = c.post("/main/graphs/", {"year": ["2017"]})
        self.assertContains(response, "Choose a Year")

    def test_service_users_list(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        response = c.post("/main/service_users_list/", {"search_text": "Yram"})
        self.assertContains(response, "Mary")
        self.assertNotContains(response, "John")
        # Test sorting
        response = c.post("/main/service_users_list/", {"sort_by": "-referral__last_name"})
        self.assertContains(response, "Mary")

    def test_referrals_list(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        response = c.get("/main/referrals_list/")
        self.assertContains(response, "Nota Usher")
        self.assertNotContains(response, "Mary")

    def test_referrals_list_search(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        response = c.post("/main/referrals_list/", {"search_text": "Nota"})
        self.assertContains(response, "Nota")
        self.assertNotContains(response, "Mary")
        # Test sorting
        response = c.post("/main/referrals_list/", {"sort_by": "last_name"})
        self.assertContains(response, "Nota")

    def test_referral_page_no_service_user(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        response = c.get("/main/referral/"+str(Referral.objects.get(first_name="Nota").id)+"/")
        self.assertContains(response, "Accept referral")

    def test_referral_page_service_user(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        response = c.get("/main/referral/"+str(Referral.objects.get(first_name="Mary").id)+"/", follow=True)
        self.assertNotContains(response, "Accept referral")

class NoServiceUsersTest(TestCase):
    def setUp(self):
        User.objects.create_user(username="Turnip", password="haggis")

    def test_graphs_no_service_users(self):
        c = Client()
        c.login(username="Turnip", password="haggis")
        response = c.get("/main/graphs/")
        self.assertContains(response, "There are currently no registered service users.")