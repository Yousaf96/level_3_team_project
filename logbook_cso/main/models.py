from django.db import models
from django.contrib.auth.models import User
import datetime

# User is default Django user

class Referral(models.Model):
    # Gender choices
    MALE_CHOICE = 'M'
    FEMALE_CHOICE = 'F'
    OTHER_CHOICE = 'O'
    GENDER_CHOICES = (
        (MALE_CHOICE, 'Male'),
        (FEMALE_CHOICE, 'Female'),
        (OTHER_CHOICE, 'Other/Rather not disclose')
    )

    # ID - default
    # NIN
    nin = models.CharField(null=True, blank=True, max_length=10)
    # First name
    first_name = models.CharField(blank=False, max_length=128)
    # Last name
    last_name = models.CharField(blank=False, max_length=128)
    # Phone number
    phone = models.CharField(null=True, blank=True, max_length=16)
    # Email address
    email = models.EmailField(null=True, blank=True, max_length=128)
    # Date of birth
    dob = models.DateField()
    # Address
    address = models.TextField(blank=False, max_length=1024)
    # Referral date
    referral_date = models.DateField()
    # Referred by
    referred_by = models.CharField(null=True, blank=True, max_length=256)
    # Contact name
    contact_name = models.CharField(null=True, blank=True, max_length=256)
    # Contact phone
    contact_phone = models.CharField(null=True, blank=True, max_length=256)
    # Gender
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES, default=OTHER_CHOICE)
    # Reason
    reason = models.TextField(null=True, blank=True, max_length=1024)
    # Notes
    notes = models.TextField(null=True, blank=True, max_length=1024)
    # Doctor's name
    doctor_name = models.CharField(null=True, blank=True, max_length=256)
    # Doctor's phone
    doctor_phone = models.CharField(null=True, blank=True, max_length=16)
    # Doctor surgery address
    surgery_address = models.TextField(null=True, blank=True, max_length=1024)
    # If the user is banned
    is_banned = models.BooleanField(default=False)

    def __str__(self):
        return str(self.first_name) + " " + str(self.last_name)

    class Meta:
        ordering = ('-referral_date',)


class Service(models.Model):
    #The type of service
    service_type = models.CharField(max_length=64)
    #Description of the service
    description = models.TextField(null=True, blank=True, max_length=1024)

    def __str__(self):
        return self.service_type


class ServiceUser(models.Model):
    # ID - default
    # Date accepted
    date_accepted = models.DateField(null=False)
    # Health issues
    health_issues = models.TextField(null=True, blank=True)
    # Mental issues
    mental_issues = models.TextField(null=True, blank=True)
    # Drug issues
    drug_issues = models.TextField(null=True, blank=True)
    # Criminal background
    criminal_background = models.TextField(null=True, blank=True)
    # Alcohol issues
    alcohol_issues = models.TextField(null=True, blank=True)
    # Emotional issues
    emotional_issues = models.TextField(null=True, blank=True)
    # Is the service user pregnant
    is_pregnant = models.BooleanField(default=False)
    # Relation with the Referral class
    referral = models.OneToOneField(Referral, on_delete=models.CASCADE)

    def __str__(self):
        return self.referral.__str__()

    class Meta:
        ordering = ('-date_accepted',)


class UserUsesService(models.Model):
    #Service
    service = models.ForeignKey(Service, on_delete=models.CASCADE)
    #Service user
    service_user = models.ForeignKey(ServiceUser, on_delete=models.CASCADE)
    #Start date
    start_date = models.DateField(auto_now_add=True)


class LogEntry(models.Model):
    #Priority choices
    GREEN_PRIORITY = 'green'
    AMBER_PRIORITY = 'yellow'
    RED_PRIORITY = 'red'
    PRIORITY_CHOICES = (
        (GREEN_PRIORITY, 'Green'),
        (AMBER_PRIORITY, 'Yellow'),
        (RED_PRIORITY, 'Red')
    )

    #Log entry timestamp
    timestamp = models.DateTimeField(auto_now_add=True)
    #Priority
    priority = models.CharField(max_length=6,choices=PRIORITY_CHOICES, default=GREEN_PRIORITY)
    #Title
    title = models.CharField(max_length=256)
    #Description
    description = models.TextField(null=True, blank=True, max_length=1024)
    #Action required
    action_required = models.BooleanField(default=False)
    #Staff assigned to it
    assigned_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='assigned_user')
    #Service users involved
    service_users = models.ManyToManyField(ServiceUser, blank=True)
    #If this is a new version of a previous log entry
    previous_log_entry = models.OneToOneField("self", null=True, blank=True, default=None)
    #If the log entry is specifically related to a service
    service = models.ForeignKey(Service, blank=True, null=True)

    def has_previous_log_entry(self):
        return (self.previous_log_entry != None)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = 'Log entries'
        ordering = ('-timestamp',)


class DiaryEntry(models.Model):
    #Appointment date
    appointment_date = models.DateField()
    #Details
    details = models.TextField(max_length=1024)
    #Service user
    service_user = models.ForeignKey(ServiceUser, on_delete=models.CASCADE, null=True, blank=True)
    #Staff user
    staff_user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.appointment_date)

    class Meta:
        verbose_name_plural = 'Diary entries'

class IncidentReport(models.Model):
    #Title
    title = models.CharField(max_length=256)
    #Date of incident
    incident_date = models.DateField()
    #Time of incident
    incident_time = models.TimeField()
    #Location of incident
    location = models.CharField(null=True, blank=True, max_length=256)
    #Brief description
    brief_description = models.CharField(null=True, blank=True, max_length=256)
    #Names of witnesses
    witnesses = models.TextField(null=True, blank=True, max_length=1024)
    #Apparent cause of incident
    cause = models.TextField(null=True, blank=True, max_length=1024)
    #Description of the incident
    description = models.TextField(null=True, blank=True, max_length=1024)
    #Action taken
    action_taken = models.TextField(null=True, blank=True, max_length=1024)
    #Police details
    police_details = models.TextField(null=True, blank=True, max_length=1024)
    #Medical assistance details
    medical_assistance_details = models.TextField(null=True, blank=True, max_length=1024)
    #Date of staff member debriefing
    debriefing_meeting_date = models.DateField()
    #Comments on action taken during incident
    comments_action_taken = models.TextField(null=True, blank=True, max_length=1024)
    #Comments on action required
    comments_action_required = models.TextField(null=True, blank=True, max_length=1024)
    #Proposed action brief
    brief_proposed_action = models.CharField(null=True, blank=True, max_length=256)
    #Linked log entry
    log_entry = models.OneToOneField(LogEntry, on_delete=models.CASCADE)

    def __str__(self):
        return self.title


class RiskAssessment(models.Model):

    #Risk level choices
    LOW_RISK = 1
    MEDIUM_RISK = 2
    HIGH_RISK = 3
    RISK_CHOICES = (
        (LOW_RISK, 'Low'),
        (MEDIUM_RISK, 'Medium'),
        (HIGH_RISK, 'High')
    )

    #Date of risk assessment
    date_assessed = models.DateField(default=datetime.date.today)
    #Review date
    review_date = models.DateField()
    #Activity for which risk assessment is being made
    activity = models.TextField(max_length=512)
    #Potential hazards
    hazards = models.TextField(max_length=1024, null=True, blank=True)
    #Existing control measures
    existing_control = models.TextField(max_length=1024, null=True, blank=True)
    #Additional control measures
    additional_control = models.TextField(max_length=1024, null=True, blank=True)
    #Initial risk assessment
    initial_risk = models.IntegerField(choices=RISK_CHOICES)
    #Residual risk assessment
    residual_risk = models.IntegerField(choices=RISK_CHOICES)
    #Service users assessed
    assessed_service_users = models.ManyToManyField(ServiceUser)
    #Staff users involved
    staff_users = models.ManyToManyField(User)
    #Linked log entry
    log_entry = models.OneToOneField(LogEntry, on_delete=models.CASCADE)


class NeighbourComplaints(models.Model):

    #Current date
    current_date = models.DateField(default=datetime.date.today)
    #Date of complaint
    complaint_date = models.DateField()
    #Time of complaint
    complaint_time = models.TimeField()
    #Name of complainant
    complainant_name = models.CharField(max_length=128)
    #Address of complainant
    complainant_address = models.TextField(max_length=1024, null=True, blank=True)
    #Telephone No. of complainant
    complainant_no = models.CharField(null=True, blank=True, max_length=16)
    #Staff user
    staff_user = models.ForeignKey(User, on_delete=models.CASCADE)
    #How was the complaint made
    complaint_type = models.CharField(max_length=256, null=True, blank=True)
    #Staff memner who recieved complaint
    recieved_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name='neighbour_complaints_recieved')
    #Name of who complaint is regarding
    people_involved = models.CharField(max_length=1024, null=True, blank=True)
    #Address of where complaint is regarding
    complaint_address = models.TextField(max_length=1024, null=True, blank=True)
    #Telephone No. of complaint
    complaint_no = models.CharField(null=True, blank=True, max_length=16)
    #Complaint details
    complaint_details = models.TextField(max_length=1024, null=True, blank=True)
    #Managers response to complaint
    managers_response = models.TextField(max_length=1024, null=True, blank=True)
    #Feedback
    feedback = models.TextField(max_length=1024, null=True, blank=True)
    #Linked log entry
    log_entry = models.OneToOneField(LogEntry, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = 'Neighbour Complaints'

class SocialWork(models.Model):

    #Name of Staff user
    staff_user = models.ForeignKey(User, on_delete=models.CASCADE)
    #Current date
    current_date = models.DateField(default=datetime.date.today)
    #Social Work office contacted
    office_contacted = models.CharField(max_length=256)
    #Social worker name
    social_worker = models.CharField(max_length=128)
    #Summary of concern raised
    concern_summary = models.TextField(max_length=1024, null=True, blank=True)
    #Social work response/outcome
    response = models.TextField(max_length=1024, null=True, blank=True)
    #Linked log entry
    log_entry = models.OneToOneField(LogEntry, on_delete=models.CASCADE)

class VulnerableYoungAdultConcerns(models.Model):

    #Name of support worker
    support_worker = models.CharField(max_length=128)
    #Tenants name
    tenants_name = models.CharField(max_length=128, null=True, blank=True)
    #Address
    address = models.TextField(max_length=1024, null=True, blank=True)
    #Date of birth
    date_of_birth = models.DateField(null=True, blank=True)
    #Current date
    current_date = models.DateField(default=datetime.date.today)
    #Concerns
    concerns = models.TextField(max_length=1024, null=True, blank=True)
    #Managers name
    managers_name = models.CharField(max_length=128, null=True, blank=True)
    #Manager informed date
    informed_date = models.DateField(null=True, blank=True)
    #Comments
    comments = models.TextField(max_length=1024, null=True, blank=True)
    #Linked log entry
    log_entry = models.OneToOneField(LogEntry, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = 'Vulnerable Young Adult Concerns'

class ChildProtectionConcern(models.Model):

    #Concern raiser name
    raiser_name = models.CharField(max_length=128)
    #Raiser address
    raiser_address = models.TextField(max_length=1024, null=True, blank=True)
    #Telephone number
    phone_number = models.CharField(null=True, blank=True, max_length=16)
    #Name of child
    child_name = models.CharField(max_length=128, null=True, blank=True)
    #Child date of birth
    date_of_birth = models.DateField(null=True, blank=True)
    #Mothers name
    mothers_name = models.CharField(max_length=128, null=True, blank=True)
    #Mothers date of birth
    mothers_date_of_birth = models.DateField(null=True, blank=True)
    #Mothers address
    mothers_address = models.TextField(max_length=1024, null=True, blank=True)
    #Fathers name
    fathers_name = models.CharField(max_length=128, null=True, blank=True)
    #Fathers date of birth
    fathers_date_of_birth = models.DateField(null=True, blank=True)
    #Fathers address
    fathers_address = models.TextField(max_length=1024, null=True, blank=True)
    #Home address
    home_address = models.TextField(max_length=1024, null=True, blank=True)
    #Additional depedents
    additional_dependents = models.TextField(max_length=1024, null=True, blank=True)
    #Other professionals
    other_professionals = models.TextField(max_length=1024, null=True, blank=True)
    #Other professionals informed
    other_professionals_informed = models.TextField(max_length=1024, null=True, blank=True)
    #Concern recorder
    concern_recorder = models.CharField(max_length=128, null=True, blank=True)
    #Date of concern
    date_of_concern = models.DateField(null=True, blank=True)
    #Concern
    concern = models.TextField(max_length=1024, null=True, blank=True)
    #Action taken
    action_taken = models.TextField(max_length=1024, null=True, blank=True)
    #Managers name
    managers_name = models.CharField(max_length=128, null=True, blank=True)
    #Manager informed date
    informed_date = models.DateField(null=True, blank=True)
    #Comments
    comments = models.TextField(max_length=1024, null=True, blank=True)
    #Action required
    action_required = models.TextField(max_length=1024, null=True, blank=True)
    #Outcome
    outcome = models.TextField(max_length=1024, null=True, blank=True)
    #Linked log entry
    log_entry = models.OneToOneField(LogEntry, on_delete=models.CASCADE)
