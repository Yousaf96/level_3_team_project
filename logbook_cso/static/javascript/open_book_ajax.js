$(document).ready(function(){
    $(".get_log_details").click(function(){
        log_details(this);
    });
});

//function to show the additional details of a log entry
function log_details(x){
    var div_id = $(x).attr('data-id');
    var log_id = $(x).attr('data-id');
    if($('#log_'+div_id).is(':empty')){
        $.get('/main/log_details/', {log:log_id}, function(data){
            $('#log_'+div_id).html(data['html_log']);
            $('#show_additional_'+div_id).hide();
            $('#hide_additional_'+div_id).removeAttr("hidden");
            $('#hide_additional_'+div_id).show();
        });
    }else{
         $('#log_'+div_id).html("");
         $('#show_additional_'+div_id).show();
         $('#hide_additional_'+div_id).hide();
    }

}