Open Door Accommodation Project - Logbook application, version 1.0

This is the web application developed by the Glasgow University CSO team for the Open Door Accommodation project.

This application offers the following functions:
- log book containing log entries, including search functionality
- service users list and service user page
- referrals list and referral page
- diary showing scheduled events
- statistics about service users
- additional details for log entries, e.g. risk assessments, incident reports
- forms for adding any of the above data to the database
- admin interface offering complete control over database contents, including staff accounts

For complete user documentation please see the docs folder.


Technical instructions for running the server:
- requires Python with pip; Python 3 recommended
- note that based on your Python install, you might have to run it with python3 instead of python and pip3 instead of pip
- make sure to install required libraries by running 'pip install -r requirements.txt'
- if you are running for the first time or have made changes to the models, migrate them to the
  database with 'python manage.py makemigrations main' and 'python manage.py migrate'
- if running locally, run the server with 'python manage.py runserver'
- if running on heroku or another server, we strongly recommend you use gunicorn instead:
  'gunicorn --bind 0.0.0.0:<PORT> logbook_cso.wsgi'; port would usually be 80

 Note: this is the master version of the application, to be used when running on a server
 This version should run on normal machines as well without issues; if not, please use the 'run locally' version instead

